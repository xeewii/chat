package client;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.swing.text.html.parser.Parser;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.util.ArrayList;

public class ServerHandlerXML implements ServerHandler {
    private DataInputStream inMessage;
    private DataOutputStream outMessage;
    Element element = null;

    ServerHandlerXML(InputStream inMessage, OutputStream outMessage){
        this.inMessage = new DataInputStream(inMessage);
        this.outMessage = new DataOutputStream(outMessage);
    }

    public void writeMessage(String message){
        StringBuilder msg = new StringBuilder();
        msg.append("<command name=\"message\">\n<message>");
        msg.append(message);
        msg.append("</message>\n</command>");
        write(msg.toString());
    }

    public void writeUserLogin(String userLogin){
        StringBuilder msg = new StringBuilder();
        msg.append("<command name=\"login\">\n<name>");
        msg.append(userLogin);
        msg.append("</name>\n</command>");
        write(msg.toString());
    }

    public void writeList(){
        StringBuilder msg = new StringBuilder();
        msg.append("<command name=\"list\">\n<session>");
        msg.append(1);
        msg.append("</session>\n</command>");
        write(msg.toString());
    }

    public void writePong(){
        StringBuilder msg = new StringBuilder();
        msg.append("<command name=\"ping\">\n<name>");
        msg.append("pong");
        msg.append("</name>\n</command>");
        write(msg.toString());
    }

    public void write(String message){
        try{
            byte[] data = message.getBytes();
            outMessage.writeInt(data.length);
            outMessage.write(data);
        }
        catch(IOException ex){
            ex.printStackTrace();
        }
    }

    public void readMessage() throws IOException{
        try {
            DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            int size = inMessage.readInt();
            byte[] data = inMessage.readNBytes(size);
            Document document = builder.parse(new ByteArrayInputStream(data));
            element = document.getDocumentElement();
        }
        catch (SAXException ex){
            ex.printStackTrace();
        }
        catch (ParserConfigurationException ex){
            ex.printStackTrace();
        }
    }

    public ArrayList<String> readList(){
        ArrayList<String> names = new ArrayList<>();
        NodeList list = element.getChildNodes();
        for (int i = 0; i < list.getLength(); i++) {
            if (list.item(i).getNodeName().equals("listusers")) {
                NodeList list2 = list.item(i).getChildNodes();
                for (int j = 0; j < list2.getLength(); j++) {
                    if (list2.item(j).getNodeName().equals("user")) {
                        NodeList list3 = list2.item(j).getChildNodes();
                        for (int k = 0; k < list3.getLength(); k++) {
                            if (list3.item(k).getNodeName().equals("name")) {
                                names.add(list3.item(k).getTextContent());
                            }
                        }
                    }
                }
            }
        }
        return names;
    }

    public String getCommand(){
        return element.getAttributeNode("name").getNodeValue();
    }

    public String getMessage(){
        NodeList list = element.getChildNodes();
        for (int i = 0; i < list.getLength(); i++) {
            if (list.item(i).getNodeName().equals("message")) {
                return list.item(i).getTextContent();
            }
        }
        return null;
    }

}
