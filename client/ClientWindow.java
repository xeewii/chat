package client;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;

class ClientWindow extends JFrame {
    private JTextArea jtaMessage = new JTextArea();
    private JTextArea jtaClients = new JTextArea();

    ClientWindow(Client client) {
        setBounds(600,100,600,500);
        setTitle("client");
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setLayout(new BorderLayout());
        JDialog jd =new JDialog(this);
        jd.setTitle("Login");
        jd.setLocationRelativeTo(this);
        jd.setSize(300,100);
        jd.setModal(true);
        jd.setResizable(false);

        JTextField jtfName = new JTextField("write name");
        jtfName.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                jtfName.setText("");
            }

            @Override
            public void focusLost(FocusEvent e) {

            }
        });
        jd.add(jtfName);

        JPanel jpClients = new JPanel(new BorderLayout());
        add(jpClients, BorderLayout.EAST);

        JLabel jlUserName = new JLabel();
        jlUserName.setBackground(Color.LIGHT_GRAY);
        jpClients.add(jlUserName, BorderLayout.NORTH);

        JButton jbSerial = new JButton("Serialization");
        jbSerial.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (!jtfName.getText().equals("")) {
                    client.setTypeMessage("serial");
                    client.setNameClient(jtfName.getText());
                    jlUserName.setText(jtfName.getText());
                    jd.setVisible(false);
                }
            }
        });
        JButton jbXML = new JButton("XML");
        jbXML.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (!jtfName.getText().equals("")) {
                    client.setTypeMessage("XML");
                    client.setNameClient(jtfName.getText());
                    jlUserName.setText(jtfName.getText());
                    jd.setVisible(false);
                }
            }
        });


        jd.setLayout(new FlowLayout(FlowLayout.CENTER));

        jd.add(jbSerial);
        jd.add(jbXML);

        jd.setVisible(true);

        jtaMessage.setBackground(Color.blue);
        jtaMessage.setEditable(false);
        jtaMessage.setLineWrap(true);
        JScrollPane jScrollPane = new JScrollPane(jtaMessage);
        add(jScrollPane,BorderLayout.CENTER);

        jtaClients.setBackground(Color.CYAN);
        jtaClients.setEditable(false);
        jtaClients.setLineWrap(true);
        jpClients.add(jtaClients, BorderLayout.CENTER);

        JPanel bottomPanel = new JPanel(new BorderLayout());
        add(bottomPanel, BorderLayout.SOUTH);

        JTextField jtfMessage = new JTextField("Введите сообщение: ");
        jtfMessage.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                jtfMessage.setText("");
            }

            @Override
            public void focusLost(FocusEvent e) {

            }
        });

        bottomPanel.add(jtfMessage, BorderLayout.CENTER);

        JButton jbSendMessage = new JButton("Отправить");
        jbSendMessage.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(client.getWork()) {
                    String message = jtfMessage.getText();
                    if (!message.equals("")) {
                        client.writeMessage(message);
                    }
                    jtfMessage.setText("");
                }
            }
        });
        bottomPanel.add(jbSendMessage, BorderLayout.EAST);

        addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {

            }

            @Override
            public void keyPressed(KeyEvent e) {
                if(e.getKeyCode() == KeyEvent.VK_ENTER){
                    String message = jtfMessage.getText();
                    if(!message.equals("")){
                        client.writeMessage(message);
                    }
                    jtfMessage.setText("");
                }
            }

            @Override
            public void keyReleased(KeyEvent e) {

            }
        });



        setVisible(true);
    }

    void writeMessage(String message){
        jtaMessage.append(message + "\n");
    }

    void writeListClients(ArrayList<String> clients){
        jtaClients.setText("");
        for(String client: clients) {
            jtaClients.append(client + "\n");
        }
    }

    void writeErrorServer(){
        jtaMessage.append("Server error\n");
    }

}


