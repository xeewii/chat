package client;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

public class ServerHandlerSerial implements ServerHandler {
    private ObjectInputStream inMessage;
    private ObjectOutputStream outMessage;
    private String message;

    ServerHandlerSerial(ObjectInputStream inMessage, ObjectOutputStream outMessage){
        this.inMessage = inMessage;
        this.outMessage = outMessage;
    }

    public void writeMessage(String message){
        write("message#" + message);
    }

    public void writeUserLogin(String message){
        write("userLogin#" + message);
    }

    public void writeList(){
        write("list#");
    }

    public void writePong() {
        write("pong#");
    }

    public void write(String message){
        try{
            outMessage.writeObject(message);
            outMessage.flush();
        }
        catch(IOException ex){
            ex.printStackTrace();
        }
    }

    public void readMessage() throws IOException{
        try{
            message = (String)inMessage.readObject();
        }
        catch (ClassNotFoundException ex){
            ex.printStackTrace();
        }
    }

    public ArrayList<String> readList(){
        ArrayList<String> names = new ArrayList<>();
        while (message.contains("#")) {
            String name = message.substring(0, message.indexOf("#"));
            names.add(name);
            message = message.substring(message.indexOf("#") + 1);
        }
        return names;
    }

    public String getCommand(){
        String command = message.substring(0,message.indexOf("#"));
        message = message.substring(message.indexOf("#")+1);
        return command;
    }

    public String getMessage(){
        return message; //.substring(message.indexOf("#")+1);
    }

}
