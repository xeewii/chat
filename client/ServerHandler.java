package client;

import java.io.IOException;
import java.util.ArrayList;

public interface ServerHandler {
    void writeMessage(String message);
    void writeUserLogin(String message);
    void writePong();
    void writeList();
    void write(String message);
    void readMessage() throws IOException;
    String getCommand();
    String getMessage();
    ArrayList<String> readList();
}
