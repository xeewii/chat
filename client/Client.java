package client;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;

public class Client implements Runnable{
    private static final String HOST = "localhost";
    private static final int PORT = 1234;
    private Socket clientSocket = null;
    private ServerHandler serverHandler = null;
    private ObjectInputStream inMessage = null;
    private ObjectOutputStream outMessage = null;
    private String nameClient = null;
    private ClientWindow clientWindow;
    private String typeMessage = null;
    private ArrayList<String> anotherClients = new ArrayList<>();
    private boolean work = false;

    public Client(){
        clientWindow = new ClientWindow(this);
        try{
            clientSocket = new Socket(HOST, PORT);
            inMessage = new ObjectInputStream(clientSocket.getInputStream());
            outMessage = new ObjectOutputStream(clientSocket.getOutputStream());
            outMessage.writeObject(typeMessage);
            if (typeMessage.equals("serial")){
                serverHandler = new ServerHandlerSerial(inMessage, outMessage);
            }
            else {
                serverHandler = new ServerHandlerXML(clientSocket.getInputStream(), clientSocket.getOutputStream());
            }
            work = true;
        }
        catch (IOException ex){
            clientWindow.writeErrorServer();
        }
    }

    @Override
    public void run(){
        if(work){
            serverHandler.writeUserLogin(nameClient);
            serverHandler.writeList();
            while(true) {
                try{
                    serverHandler.readMessage();
                }
                catch(IOException ex){
                    work = false;
                    clientWindow.writeErrorServer();
                    break;
                }
                String command = serverHandler.getCommand();
                if (command.equals("message")) {
                    clientWindow.writeMessage(serverHandler.getMessage());
                }
                if (command.equals("ping")) {
                    serverHandler.writePong();
                }
                if (command.equals("list")) {
                    anotherClients = serverHandler.readList();
                    clientWindow.writeListClients(anotherClients);
                }
            }
        }
    }

    boolean getWork(){
        return work;
    }

    void setWork(boolean val){
        work = val;
    }

    void setTypeMessage(String typeMessage){
        this.typeMessage = typeMessage;
    }

    void setNameClient(String nameClient) {
        this.nameClient = nameClient;
    }

    void writeMessage(String message){
        serverHandler.writeMessage(nameClient + "#" + message);
    }

    public static void main(String args[]){
        Client client = new Client();
        new Thread(client).start();
    }

}
