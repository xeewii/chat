package server;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

public class Server {
    static final int PORT = 1234;
    private ArrayList<ClientHandler> clients = new ArrayList<>();
    private ArrayList<String> lastMessage = new ArrayList<>();

    Server(){
        Socket clientSocket = null;
        ServerSocket serverSocket = null;
        try{
            serverSocket = new ServerSocket(PORT);
            System.out.println("server start");
            while(true){
                clientSocket = serverSocket.accept(); //ждем подключений от сервера
                ObjectOutputStream outMessage = new ObjectOutputStream(clientSocket.getOutputStream());
                ObjectInputStream inMessage = new ObjectInputStream(clientSocket.getInputStream());
                try{
                    String typeConnection = (String)inMessage.readObject();
                    ClientHandler client;
                    if (typeConnection.equals("serial")){
                        client = new ClientHandlerSerial(this, inMessage, outMessage);
                    }
                    else {
                        client = new ClientHandlerXML(this, clientSocket.getInputStream(), clientSocket.getOutputStream());
                    }
                    clients.add(client);
                    new Thread(client).start();
                }
                catch(Exception ex){
                    ex.printStackTrace();
                }
            }
        }
        catch(IOException ex){
            ex.printStackTrace();
        }
        finally {
            try{
                clientSocket.close();
                System.out.println("server stop");
                serverSocket.close();
            }
            catch (IOException ex){
                ex.printStackTrace();
            }
        }
    }

    void sendMessageToAllClients(String message){
        if(lastMessage.size() > 3) lastMessage.remove(0);
        lastMessage.add(message);
        for(ClientHandler client: clients){
            client.sendMessage(message);
        }
    }

    void sendList(String message){
        for(ClientHandler client: clients){
            client.sendList(message);
        }
    }

    void removeClient (ClientHandler client){
        clients.remove(client);
        sendList(client.getNameClients());
    }

    ArrayList<String> getLastMessage(){
        return lastMessage;
    }

    ArrayList<String> getNamesClients(){
        ArrayList<String> names = new ArrayList<>();
        for(ClientHandler clientHandler: clients){
            names.add(clientHandler.getNameClient());
        }
        return names;
    }

    public static void main(String argv[]){
        Server server = new Server();
    }

}
