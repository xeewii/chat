package server;

public interface ClientHandler extends Runnable {
    @Override
    void run();

    String getNameClient();
    String getNameClients();
    void sendMessage(String message);
    void sendPing();
    void sendList(String list);
    void sendLastMessage();
    void close();
    boolean getPingFlag();
    void setPingFlag(boolean flag);
    void clientDead();

}
