package server;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

public class ClientHandlerSerial implements ClientHandler {
    private Server server;
    private ObjectInputStream inMessage;
    private ObjectOutputStream outMessage;
    private Ping ping;
    private String nameClient = null;
    private volatile boolean pingFlag = false;

    ClientHandlerSerial(Server server, ObjectInputStream inMessage, ObjectOutputStream outMessage){
        this.server = server;
        this.inMessage = inMessage;
        this.outMessage = outMessage;
        ping = new Ping(this);
        new Thread(ping).start();
    }

    @Override
    public void run(){
        try{
            while(true) {
                String message = readMessage();
                String command = message.substring(0,message.indexOf("#"));
                String msg = message.substring(message.indexOf("#")+1);
                System.out.println(command);
                System.out.println(msg);
                if(command.equals("message")) {
                    server.sendMessageToAllClients(msg);
                }
                if(command.equals("pong")){
                    pingFlag = true;
                }
                if(command.equals("list")){
                    server.sendList(getNameClients());
                }
                if (command.equals("end")){
                    break;
                }
                if(command.equals("userLogin"))
                {
                    nameClient = msg;
                    sendLastMessage();
                    server.sendMessageToAllClients(nameClient + " come to chat!");
                }
                Thread.sleep(100);
            }
        }
        catch(InterruptedException ex){
            ex.printStackTrace();
        }
        finally {
            this.close();
        }
    }

    private String readMessage(){
        try{
            return (String)inMessage.readObject();
        }
        catch(Exception ex){
            ex.printStackTrace();
            return null;
        }
    }

    public void sendMessage(String message){
        try{
            outMessage.writeObject("message#" + message);
            outMessage.flush();
        }
        catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public void sendLastMessage(){
        ArrayList<String> lastMessage = server.getLastMessage();
        for(String message:lastMessage){
            sendMessage(message);
        }
    }

    public void sendList(String message){
        try{
            outMessage.writeObject("list#" + message);
            outMessage.flush();
        }
        catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public void sendPing(){
        try{
            outMessage.writeObject("ping#");
            outMessage.flush();
        }
        catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public void close(){
        server.removeClient(this);
        server.sendMessageToAllClients(nameClient + " disconnected");
    }

    public void setPingFlag(boolean flag){
        pingFlag = flag;
    }

    public boolean getPingFlag(){
        return pingFlag;
    }

    public void clientDead(){
        server.removeClient(this);
    }

    public String getNameClient(){
        return nameClient;
    }

    public String getNameClients(){
        ArrayList<String> users = server.getNamesClients();
        String names = "";
        for(String name: users){
            names = names + name + "#";
        }
        return names;
    }
}
