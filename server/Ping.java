package server;

import static java.lang.Thread.sleep;

public class Ping implements Runnable {
    private ClientHandler clientHandler;

    Ping(ClientHandler clientHandler){
        this.clientHandler = clientHandler;
    }

    @Override
    public void run(){
        try {
            while (true) {
                sleep(3000);
                clientHandler.sendPing();
                sleep(5000);
                if(clientHandler.getPingFlag()){
                    clientHandler.setPingFlag(false);
                }
                else {
                    clientHandler.clientDead();
                    break;
                }
            }
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
    }

}
