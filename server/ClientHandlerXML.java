package server;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.*;
import java.util.ArrayList;

public class ClientHandlerXML implements ClientHandler {
    private Server server;
    private DataInputStream inMessage;
    private DataOutputStream outMessage;
    private Ping ping;
    private String nameClient = null;
    private volatile boolean pingFlag = false;

    ClientHandlerXML(Server server, InputStream inMessage, OutputStream outMessage){
        this.server = server;
        this.inMessage = new DataInputStream(inMessage);
        this.outMessage = new DataOutputStream(outMessage);
        ping = new Ping(this);
        new Thread(ping).start();
    }

    @Override
    public void run(){
        try{
            while(true) {
                DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
                int size = inMessage.readInt();
                byte[] data = inMessage.readNBytes(size);
                Document document = builder.parse(new ByteArrayInputStream(data));
                Element element = document.getDocumentElement();
                if (element.getNodeName().equals("command")) {
                    String command = element.getAttributeNode("name").getNodeValue();
                    if (command.equals("login")) {
                        NodeList list = element.getChildNodes();
                        for (int i = 0; i < list.getLength(); i++) {
                            if (list.item(i).getNodeName().equals("name")) {
                                nameClient = list.item(i).getTextContent();
                            }
                        }
                        sendLastMessage();
                        server.sendMessageToAllClients(nameClient + " come to chat!");
                    }
                    if(command.equals("ping")){
                        pingFlag = true;
                    }
                    if (command.equals("message")) {
                        String message = null;
                        NodeList list = element.getChildNodes();
                        for (int i = 0; i < list.getLength(); i++) {
                            if (list.item(i).getNodeName().equals("message")) {
                                message = list.item(i).getTextContent();
                            }
                        }
                        server.sendMessageToAllClients(message);
                    }
                    if(command.equals("list")){
                        server.sendList(getNameClients());
                    }
                }
                Thread.sleep(100);
            }
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
        finally {
            this.close();
        }
    }

    public String getNameClient(){
        return nameClient;
    }

    public void sendList(String list){
        StringBuilder names = new StringBuilder();
        names.append("<command name=\"list\">\n");
        names.append(list);
        names.append("\n</command>");
        write(names.toString());
    }

    public String getNameClients(){
        ArrayList<String> users = server.getNamesClients();
        StringBuilder names = new StringBuilder();
        names.append("<listusers>\n");
        for(String name: users){
            names.append("<user>\n<name>");
            names.append(name);
            names.append("</name>\n</user>\n");
        }
        names.append("</listusers>");
        return names.toString();
    }

    public void sendMessage(String message){
        StringBuilder msg = new StringBuilder();
        msg.append("<command name=\"message\">\n<message>");
        msg.append(message);
        msg.append("</message>\n</command>");
        write(msg.toString());
    }

    public void sendLastMessage(){
        ArrayList<String> lastMessage = server.getLastMessage();
        for(String message:lastMessage){
            sendMessage(message);
        }
    }

    public void sendPing(){
        StringBuilder msg = new StringBuilder();
        msg.append("<command name=\"ping\">\n<message>");
        msg.append("ping");
        msg.append("</message>\n</command>");
        write(msg.toString());
    }

    private void write(String message){
        try{
            byte[] data = message.getBytes();
            outMessage.writeInt(data.length);
            outMessage.write(data);
        }
        catch(IOException ex){
            ex.printStackTrace();
        }
    }

    public void close(){
        server.removeClient(this);
        server.sendMessageToAllClients(nameClient + " disconnected");
    }

    public void setPingFlag(boolean flag){
        pingFlag = flag;
    }

    public boolean getPingFlag(){
        return pingFlag;
    }

    public void clientDead(){
        server.removeClient(this);
    }
}
